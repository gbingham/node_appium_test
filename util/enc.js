var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var password = 'ip5$VQnvfC0W#6H9$uiS9^yMt';

module.exports = function(text) {
  var cipher = crypto.createCipher(algorithm, password);
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
};

if (process.argv[0].match(/node/) && !process.argv[1].match(/mocha/)) {
  var prompt = require('prompt');
  var schema = {
    properties: {
      password: {
        hidden: true
      }
    }
  };
  prompt.start();
  prompt.get(schema, function(err, result) {
    console.log('input received');
    console.log('encrypted password is : ' + encrypt(result.password));
    console.log('please paste the encrypted password into your override.json file');
    process.exit();
  });
}
