var expect = require('chai').expect;
var parseArgs = require('minimist');

describe('cliArgs.js', function() {
  beforeEach(function () {
    //require has a cache. Who knew?!?!
    delete require.cache[require.resolve('../../lib/cliArgs.js')];
  });
  it('should return arg in form of --key=value with an expected value', function() {
    process.argv = ['--brand=bluehost'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.brand).to.equal('bluehost');
  });
  it('should return brand in form of -k value with an expected value', function() {
    process.argv = ['-b bluehost'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.brand).to.equal('bluehost');
  });
  it('should return arg in the form of --key=value with no expected value', function() {
    process.argv = ['--domain=anystring'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.domain).to.equal('anystring');
  });
  it('should return domain in form of -k value with no expected value', function() {
    process.argv = ['-d anystring'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.domain).to.equal('anystring');
  });
  it('should return a boolean based argument like --baseline or --verbose', function() {
    process.argv = ['--baseline', '--verbose'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.baseline).to.equal(true);
    expect(cliArgs.verbose).to.equal(true);
  });
  it('should return a number in the form of --cid=1', function() {
    process.argv = ['--cid=1'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.custId).to.equal('1');
  });
  it('should return a number in the form of -i 1', function() {
    process.argv = ['-i 1'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.custId).to.equal('1');
  });
  it('should set all the things', function() {
    process.argv = ['--verbose', '--baseline', '--brand=justhost', '-d domain.com', '--type=shared', '--browser=firefox', '--id=2'];
    var cliArgs = require('../../lib/cliArgs.js');
    expect(cliArgs.brand).to.equal('justhost');
    expect(cliArgs.domain).to.equal('domain.com');
    expect(cliArgs.custId).to.equal('2');
    expect(cliArgs.type).to.equal('shared');
    expect(cliArgs.browser).to.equal('firefox');
    expect(cliArgs.baseline).to.equal(true);
    expect(cliArgs.verbose).to.equal(true);
  });
});
