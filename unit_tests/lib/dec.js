const expect = require('chai').expect;
var x = require('../../lib/dec.js');
describe('dec.js', function() {
  it('decrypt should be a function', function() {
    expect(typeof x).to.equal('function');
  });
  it('decrypt should decrypt a string', function() {
    expect(x('ef1b4570af8ed0')).to.equal('test123');
  });
});
