var webdriverio = require('webdriverio');
var expect = require('chai').expect;
var options = {
    desiredCapabilities: {
    	platformName: 'iOS',                        // operating system
        app: 'com.apx.TechGenie',            // bundle id of the app
        appActivity: 'com.apx.TechGenie',// app activity, which should be started
        avdReadyTimeout: '1000',                        // waiting time for the app to start
        udid: 'C648B1EE-B6A2-498C-96EA-6F5CCEC7411D',                           // udid of the android device
        automationName : 'XCUITest',
		deviceName: 'TG-464 SG-400',                         // device name
    },
    host: 'localhost',                                  // localhost
    port: 4723  
};

var client = webdriverio.remote(options);

describe('first test', function () {
    this.timeout(9999999);
    before(function () {
        return client.init();
    });
    it('should do get the device time', function () {
        return client.getDeviceTime().then(function (time) {
            console.log(time);
        })
    });
    after(function () {
        return client.end();
    });
});
